package io.buchin.navigationexample.features.tabpartners

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import io.buchin.navigationexample.R
import kotlinx.android.synthetic.main.fragment_screen.button_next
import kotlinx.android.synthetic.main.fragment_screen.textview

class PartnersFragment : Fragment() {

  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? = View.inflate(
      activity,
      R.layout.fragment_screen,
      null
  )

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    textview?.text = "PartnersFragment"
    button_next?.apply {
      visibility = View.VISIBLE
      setOnClickListener {
        findNavController().navigate(R.id.action_partnersFragment_to_partnersNextFragment, bundleOf("title" to "PartnersNextFragment new title"))
      }
    }
  }

}