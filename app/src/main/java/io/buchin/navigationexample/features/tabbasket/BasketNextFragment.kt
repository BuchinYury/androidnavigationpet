package io.buchin.navigationexample.features.tabbasket

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import io.buchin.navigationexample.R
import kotlinx.android.synthetic.main.fragment_screen.button_back
import kotlinx.android.synthetic.main.fragment_screen.textview

class BasketNextFragment : Fragment() {

  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? = View.inflate(
      activity,
      R.layout.fragment_screen,
      null
  )

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    textview?.text = "BasketNextFragment"

    button_back?.apply {
      visibility = View.VISIBLE
      setOnClickListener {
        findNavController().popBackStack()
      }
    }
  }
}