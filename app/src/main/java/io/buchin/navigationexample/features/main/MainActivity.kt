package io.buchin.navigationexample.features.main

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import io.buchin.navigationexample.R
import kotlinx.android.synthetic.main.activity_main.imageview_filters_basket
import kotlinx.android.synthetic.main.activity_main.imageview_filters_partners
import kotlinx.android.synthetic.main.activity_main.imageview_filters_profile
import kotlinx.android.synthetic.main.activity_main.section_basket_wrapper
import kotlinx.android.synthetic.main.activity_main.section_partners_wrapper
import kotlinx.android.synthetic.main.activity_main.section_profile_wrapper

class MainActivity : AppCompatActivity() {

  private val navPartnersController: NavController by lazy { findNavController(R.id.section_partners) }
  private val navBasketController: NavController by lazy { findNavController(R.id.section_basket) }
  private val navProfileController: NavController by lazy { findNavController(R.id.section_profile) }

  var currentController: NavController? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    handleBottomNavigation()
  }

  override fun onBackPressed() {
    currentController
        ?.let { if (it.popBackStack().not()) super.onBackPressed() }
        .or { super.onBackPressed() }
  }

  private fun handleBottomNavigation() {
    fun replacePartners() {
      currentController = navPartnersController

      section_partners_wrapper?.visibility = View.VISIBLE
      section_basket_wrapper.visibility = View.INVISIBLE
      section_profile_wrapper.visibility = View.INVISIBLE
    }

    fun replaceBasket() {
      currentController = navBasketController

      section_partners_wrapper?.visibility = View.INVISIBLE
      section_basket_wrapper.visibility = View.VISIBLE
      section_profile_wrapper.visibility = View.INVISIBLE
    }

    fun replaceProfile() {
      currentController = navProfileController

      section_partners_wrapper?.visibility = View.INVISIBLE
      section_basket_wrapper.visibility = View.INVISIBLE
      section_profile_wrapper.visibility = View.VISIBLE
    }

    replacePartners()

    imageview_filters_partners?.setOnClickListener {
      replacePartners()
    }

    imageview_filters_basket?.setOnClickListener {
      replaceBasket()
    }

    imageview_filters_profile?.setOnClickListener {
      replaceProfile()
    }
  }
}

fun <T> T?.or(compute: () -> T): T = this ?: compute()