package io.buchin.navigationexample.features.tabpartners

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import io.buchin.navigationexample.R
import io.buchin.navigationexample.common.extensions.unsafeLazy
import kotlinx.android.synthetic.main.fragment_screen.button_back
import kotlinx.android.synthetic.main.fragment_screen.button_next
import kotlinx.android.synthetic.main.fragment_screen.textview

class PartnersNextFragment : Fragment() {

  private val title by unsafeLazy {
    arguments?.getString("title") ?: "PartnersNextFragment"
  }


  override fun onCreateView(
      inflater: LayoutInflater,
      container: ViewGroup?,
      savedInstanceState: Bundle?
  ): View? = View.inflate(
      activity,
      R.layout.fragment_screen,
      null
  )

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    textview?.text = title

    button_back?.apply {
      visibility = View.VISIBLE
      setOnClickListener {
        findNavController().popBackStack()
      }
    }
  }
}